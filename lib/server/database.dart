import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:tour_of_heroes/data/server_response.dart';
import 'package:tour_of_heroes/hero_model.dart';

class Database {
  var apiUrl = 'http://10.0.2.2:3000/api/heroes';

  Future<List<HeroModel>> getHeroes() async {
    print("started get heros");
    final response = await http.get(Uri.parse(apiUrl));
    if (response.statusCode == 200) {
      print(response.body);
      List<dynamic> results = jsonDecode(response.body);
      return results.map((e) => HeroModel.fromJson(e)).toList();
    } else {
      throw Exception("Error fetching data");
    }
  }

  Future<ServerResponse> deleteHero(String id) async {
    final url = "$apiUrl/$id";
    print("started delete "+url);
    final response = await http.delete(Uri.parse(url));
    print(response.body);
    return ServerResponse.fromJson(jsonDecode(response.body));
  }

    Future  addHero(String name) async {
    HeroModel hero = HeroModel(name: name);
   // print(hero.toJson());
    final response = await http.post(Uri.parse(apiUrl),
    body: hero.toJson());
    print(response.body);
  }

  Future updateHero(String id, name) async{
    HeroModel hero = HeroModel(name: name);
    final url = "$apiUrl/$id";
    print("Updating "+url);
    final response = await http.put(Uri.parse(url),
    body: hero.toJson(),
   // headers: <String, String> {
    //  'Content-Type': 'application/json; charset=UTF-8'
   // }
    );
    //print(response.body);

  }

  Future<List<HeroModel>>  searchHero(String query) async {
    print(query);
    Map<String,String> queryParams = {
      'filter':query
    };
    print('started get search heroes');
    Uri uri = Uri.http('10.0.2.2:3000','/api/heroes',queryParams);
    final response = await http.get(uri);
    print(jsonDecode(response.body));
    if (response.statusCode == 200) {
      print(response.body);
      List<dynamic> results = jsonDecode(response.body);
      return results.map((e) => HeroModel.fromJson(e)).toList();
    } else {
      throw Exception("Error fetching data");
    }
  }

}
