

class HeroModel {
  String? name;
  String? id;

  HeroModel({required this.name, this.id});

   HeroModel.fromJson(Map<String, dynamic> json) {
    id = json['_id'];
    name = json['name'];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
   // data['_id'] = id;
    data['name'] = name;
    return data;
  }
}
