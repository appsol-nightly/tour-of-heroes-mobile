import 'package:flutter/material.dart';
import 'package:tour_of_heroes/search_hero.dart';
import 'package:tour_of_heroes/top_heroes.dart';
import 'hero_model.dart';
import 'server/database.dart';

import './add_hero.dart';

class Heroes extends StatefulWidget {
  String name;

  Heroes(this.name, {Key? key}) : super(key: key);

  @override
  _HeroesState createState() => _HeroesState();
}


class _HeroesState extends State<Heroes> {

  late Future<List<HeroModel>> _fetchHeroes;

  late TextEditingController _name = TextEditingController();


  getHeros() {
    _fetchHeroes = Database().getHeroes();
  }
  deleteHero(String id) async {
    Database().deleteHero(id)
         .then((serverResponse) {
        //   //if(serverResponse.status==200){
             ScaffoldMessenger.of(context)
                 .showSnackBar(SnackBar(content: Text(serverResponse.message)));
            setState((){
              getHeros();
             });
         // }
    }
    );
  }

  updateHero(String id){
    Database().updateHero(id , _name.text);
      setState((){
        getHeros();
      });
  }
  @override
  void initState() {
    super.initState();
    _name = TextEditingController();
    getHeros();
  }

  void _addHeroToList(String name) {

    setState(() {
    Database().addHero(name);
    getHeros();

    });
  }

  Future<void> showEditDialog(BuildContext context, HeroModel hero) async {
    final _formKey = GlobalKey<FormState>();
    return await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Update'),
          content: Form(
              key: _formKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  TextFormField(
                    controller: _name,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Name required';
                      }
                      return null;
                    },
                  )
                ],
              )),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: const Text('cancel'),
            ),
            TextButton(
              onPressed: () {
                setState(() {
                   updateHero(hero.id as String);
                  Navigator.pop(context);
                });
              },
              child: const Text('Update'),
            )
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          leading: IconButton(
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => TopHeroes()));
            },
            icon: const Icon(
              Icons.star_purple500,
              color: Colors.white,
            ),
          ),
          centerTitle: true, title: const Text('Heroes'),
          actions: [

            IconButton(
              icon: Icon(Icons.search),
              onPressed: () {
                  showSearch(context: context, delegate: SearchHero());
                })

          ]),
      body: _buildList(),
      floatingActionButton: AddHero(_addHeroToList),

    );
  }

  Widget _buildList() {
    return FutureBuilder<List<HeroModel>>(
        future: _fetchHeroes,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            print("data has arrived");
            print(snapshot.data!.length);
            return ListView.separated(
              padding: const EdgeInsets.all(8.0),
              itemCount: snapshot.data!.length,
              itemBuilder: (BuildContext context, int index) {
                var currentItem = snapshot.data![index];
                var name = currentItem.name;
                return ListTile(
                  title: Text(
                    name.toString(),
                    style: const TextStyle(fontSize: 20),
                  ),
                  trailing: SizedBox(
                      width: 96,
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          IconButton(
                            icon: const Icon(Icons.edit),
                            onPressed: () async {
                              _name.text = name.toString();
                              print(snapshot.data![index]);
                              await showEditDialog(context, snapshot.data![index]);
                            },
                          ),
                          IconButton(
                            icon: const Icon(Icons.clear_rounded),
                            onPressed: () {
                              ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(
                                  content: Text("Remove ${currentItem.name}?"),
                                  backgroundColor: Colors.black87,
                                  action: SnackBarAction(
                                    textColor: Colors.pink,
                                    onPressed: () {
                                      deleteHero(snapshot.data![index].id!);
                                    },
                                    label: "Remove",
                                  ),
                                ),
                              );
                            },
                          ),
                        ],
                      )),
                );
              },
              separatorBuilder: (BuildContext context,
                  int index) => const Divider(),
            );
          }
          else if (snapshot.hasError) {
            print(snapshot.error);
            return Container();
          }
          else {
            print("waiting for data");
            return const  Center(child: CircularProgressIndicator());
          }
        }
    );
  }
}
