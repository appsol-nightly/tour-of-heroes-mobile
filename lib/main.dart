import 'package:flutter/material.dart';
//import 'package:mongo_dart/mongo_dart.dart';

import 'server/database.dart';
import 'heroes.dart';

void main() {

  return runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  MyAppState createState() => MyAppState();
}

class MyAppState extends State<MyApp> {

  String heroName = '';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        // primaryColor: Colors.purple,
        colorScheme: ColorScheme.fromSwatch(
          primarySwatch: Colors.purple,
        ).copyWith(
          secondary: Colors.green,
        ),
      ),
      home: Heroes(heroName),

    );
  }
}
