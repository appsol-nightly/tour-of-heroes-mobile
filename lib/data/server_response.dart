class ServerResponse {
  int status;
  String message;

  ServerResponse({required this.status, required this.message});

  factory ServerResponse.fromJson(Map<String, dynamic> json) {
   return ServerResponse(status: json['status'],
   message: json['message']);

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data =  Map<String, dynamic>();
    data['status'] = status;
    data['message'] =message;
    return data;
  }
}