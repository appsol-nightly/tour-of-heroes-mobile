import 'package:flutter/material.dart';
import 'package:tour_of_heroes/server/database.dart';

import 'hero_model.dart';

class SearchHero extends SearchDelegate {
  Future<List<HeroModel>> getSearchResult(String query) async {
    return Database().searchHero(query);
  }

  @override
  List<Widget>? buildActions(BuildContext context) {
    return [
      IconButton(
          onPressed: () {
            query = "";
          },
          icon: Icon(Icons.clear))
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back_ios),
      onPressed: () {
        close(context, query);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    // List<String> heroes =
    //     _heroResults.where(
    //       (hero) => hero.toLowerCase().contains(query.toLowerCase()),
    //     ).toList();
    return FutureBuilder<List<HeroModel>>(
        future: getSearchResult(query),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ListView.separated(
              padding: const EdgeInsets.all(8),
              itemCount: snapshot.data!.length,
              itemBuilder: (BuildContext context, int index) {
                var currentIndex = snapshot.data![index];
                return ListTile(
                  title: Text(
                    (currentIndex.name).toString(),
                    style: const TextStyle(fontSize: 18),
                  ),
                  onTap: () {
                    query = (currentIndex.name).toString();
                    close(context, query);
                  },
                );
              },
              separatorBuilder: (BuildContext context, int index) =>
                  const Divider(),
            );
          } else if (snapshot.hasError) {
            return Text(snapshot.error.toString());
          } else {
            return Container();
          }
        });
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return const Center(
      child: Text('Search Hero'),
    );
  }
}
