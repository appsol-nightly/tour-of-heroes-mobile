import 'package:flutter/material.dart';
import 'package:tour_of_heroes/hero_model.dart';
import 'dart:math';

import 'package:tour_of_heroes/server/database.dart';


class TopHeroes extends StatelessWidget {
      TopHeroes( {Key? key}) : super(key: key);

   late final Future<List<HeroModel>> _topHeroes = Database().getHeroes();


   getHeroes(){
    // _topHeroes = Database().getHeroes();
     print("top: "+_topHeroes.toString());
   }

  _random(min, max){
     getHeroes();
    //_topHeroes.shuffle();
    var rn = Random();
    return min + rn.nextInt(max - min);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Top Heroes',
          textAlign: TextAlign.center,),
      ),
      body: _buildTopHeroes(),
    );
  }

  Widget _buildTopHeroes() {
    return FutureBuilder<List<HeroModel>>(
      future: _topHeroes,
      builder: (context, snapshot) {
        var maxLength = snapshot.data?.length;
        return ListView.separated(
          padding: const EdgeInsets.all(8),
          itemCount: _random(1, maxLength),
          itemBuilder: (BuildContext context, int index) {
            var currentIndex = snapshot.data![index];
            return ListTile(
              title: Text(
                (currentIndex.name).toString(),
                style: const TextStyle(
                    fontSize: 18
                ),
              ),
            );
          },
          separatorBuilder: (BuildContext context,
              int index) => const Divider(),
        );
      } );
  }
}
