import 'package:flutter/material.dart';

class AddHero extends StatefulWidget {

  final void Function(String) addHeroToList;


  const AddHero(this.addHeroToList, {Key? key}) : super(key: key);

  @override
  _AddHeroState createState() => _AddHeroState();
}

class _AddHeroState extends State<AddHero> {
  final TextEditingController controller = TextEditingController();

  late void Function(String) _addToList;

  @override
  void dispose(){
    controller.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _addToList = widget.addHeroToList;
  }

  Future<void> showAddDialog(BuildContext context) async {
    final _formKey = GlobalKey<FormState>();
    return await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Add Hero'),
          content: Form(
              key: _formKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  TextField(
                    controller: controller,
                  )
                ],
              )),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: const Text('cancel'),
            ),
            TextButton(
              onPressed: () {
                _addToList.call(controller.text);
                //_addToList(controller.text);
                Navigator.pop(context);
                setState(() {});
              },
              child: const Text('Save'),
            )
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
        child: const Icon(Icons.add),
        backgroundColor: Colors.purple[300],
        onPressed: () async {
          await showAddDialog(context);
        });
  }
}
